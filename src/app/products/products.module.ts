import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';

// import { ProductsComponent } from './products.component';


@NgModule({
  declarations: [
    ProductComponent,
    // ProductsComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    ProductComponent,
    // ProductsComponent
  ]
})
export class ProductsModule { }
