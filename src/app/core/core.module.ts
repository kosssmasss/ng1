import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AuthenticationComponent } from './authentication/authentication.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    AuthenticationComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    AuthenticationComponent
  ]
})
export class CoreModule { }
